var app = angular.module("appCart", ["ngRoute"]);

//inicializamos el controller  app 
app.controller("controllerApp", function ($scope) {
    // $scope.name="harwin Galvis"
    //planes 
    let list = this;
    list.plans = [
        { id: 0, logo_url: 'http://localhost:8080/pruebaAngular/img/logo1.png',tag:{name:'name plan', ubicacion:'cll: 34535- 65'}, restricted: true, rating_image_url: 'ratin.png', performance_image_url: 'ratin.png', fee_image_url: 'ratin.png', plan_name: 'FLorida 529 Savings Plan', descriton: 'Restricted to Florida residents Florida Preparin residen Restricted to Florida residents Florida Preparin residen vs', overal_rating: 5, performance: 0, free_score: 3, click: 4566 },
        { id: 1, logo_url: 'http://localhost:8080/pruebaAngular/img/logo2.png', restricted: true, rating_image_url: 'ratin.png', performance_image_url: 'ratin.png', fee_image_url: 'ratin.png', plan_name: 'March Vision Care Unite', descriton: 'Restricted to Florida residents Florida Preparin residen', overal_rating: 15, performance: 4, free_score: 5, click: 4566 },
        { id: 2, logo_url: 'http://localhost:8080/pruebaAngular/img/logo4.png', restricted: true, rating_image_url: 'ratin.png', performance_image_url: 'ratin.png', fee_image_url: 'ratin.png', plan_name: 'United HealthCare Oxford ', descriton: 'Restricted to Florida residents Florida Preparin residen', overal_rating: 20, performance: 1, free_score: 5, click: 4566 },
        { id: 3, logo_url: 'http://localhost:8080/pruebaAngular/img/logo5.png', restricted: true, rating_image_url: 'ratin.png', performance_image_url: 'ratin.png', fee_image_url: 'ratin.png', plan_name: 'United HealthCare Oxfor', descriton: 'Restricted to Florida residents Florida Preparin residen', overal_rating: 13.5, performance: 5, free_score: 3, click: 4566 }

    ];
    //array strellas calificadas y sin calificar.
    list.start = [];
    list.rombo = [];
    //objeto general de estrellas 
    list.starPacage = [];
    list.starToveral = [];
    list.colorRombo = [];
    //carga start 

      //carga start 

      $scope.addStart = function () {

        angular.forEach(list.plans, function(item, index) {
           
            //llena el arreilo con las estrellas calificadas 100%
            for (y = 1; y <= item.performance; y++) {
                list.start.push({ id: y, value: 100 });
            }
             //llena el arreilo con las estrellas sin calificacion 0%
            for (t = item.performance +1 ; t <= 5; t++) {
                list.start.push({ id: t, value: 0 });
            }
            list.starPacage.push({id:index, data:list.start});
            list.start=[];

          });
        $scope.addStar_toveral_rating();
        }

          $scope.addStar_toveral_rating = function () {

            angular.forEach(list.plans, function(item, index) {
                //llena el arreilo con las estrellas calificadas 100%
                for (y = 1; y <= item.free_score; y++) {
                    list.start.push({ id: y, value: 100 });
                }
                 //llena el arreilo con las estrellas sin calificacion 0%
                for (t = item.free_score +1 ; t <= 5; t++) {
                    list.start.push({ id: t, value: 0 });
                }
                list.starToveral.push({id:index, data:list.start});
                list.start=[];
    
              });


              $scope.addStar_rombo_add_color();
    
           }




//logica para visualizar rombos.
//setoma el valor overal_rating que no puede ser mayor a 20 y se crea los obj que con tiene los datos
//de las clases css que crean el obj.

    $scope.addStar_rombo_add_color = function () {

        angular.forEach(list.plans, function (item, index) {
            //llena el arreilo con las estrellas calificadas 100%
            let varRomb = 0;
        
            let numeroDerombos = 0;
            let romboNumeroParteEntera = 0;
            let romboNumeroParteDesimal = 0;

            varRomb = Math.floor(item.overal_rating);
            numeroDerombos = (varRomb / 4);
            romboNumeroParteEntera = Math.floor(numeroDerombos);
            romboNumeroParteDesimal = (numeroDerombos - Math.floor(numeroDerombos)) * 4;
            floaValue = true;


            for (y = 1; y <= romboNumeroParteEntera; y++) 
            {
                list.rombo.push({ id: y, rombo: "RomboFourFour" });
            }

            if (romboNumeroParteDesimal == 1) {
                list.rombo.push({ id: romboNumeroParteEntera+1, rombo: "RomboOnoFour" });
            } else {
                if (romboNumeroParteDesimal == 2) {
                    list.rombo.push({ id: y, rombo: "RomboTwoFour" });
                } else {
                    if (romboNumeroParteDesimal == 3) {
                    list.rombo.push({ id: y, rombo: "RomboThirdFour" });
                     }
                }
            }

            //llena el arreilo con las estrellas sin calificacion 0%
            for (t = romboNumeroParteEntera + 2; t <= 5; t++) {
                list.rombo.push({ id: t, rombo: "romboIni" });
            }
            list.colorRombo.push({ id: index, data: list.rombo });
            list.rombo = [];
           

        });

       
    }

    $scope.duplicar = function (id) {
        alert("el elemento se duplica al final de la lista"+id);
        list.plans.push({id:list.plans.length, logo_url:list.plans[id].logo_url, restricted:list.plans[id].restricted, rating_image_url:list.plans[id].rating_image_url, performance_image_url:list.plans[id].performance_image_url, fee_image_url:list.plans[id].fee_image_url, plan_name: list.plans[id].plan_name, descriton:list.plans[id].descriton, overal_rating:list.plans[id].overal_rating, performance:list.plans[id].performance, free_score:list.plans[id].free_score, click:list.plans[id].click });
        $scope.addStar_rombo_add_color();
        $scope.addStar_toveral_rating();
        $scope.addStart();
    }

    
    $scope.eliminar = function (id) {
        alert("el elemento se eliminar al final de la lista"+id);
        list.plans.splice(id, 1);
    }

});

